package com.amit.ms.store.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amit.ms.store.domain.Store;
import com.amit.ms.store.repository.StoreRepository;

@Service
public class StoreService {

	@Autowired
	private StoreRepository storeRepository;

	public List<Store> findAll() {
		return storeRepository.findAll();
	}

	public Optional<Store> findById(Integer id) {
		return storeRepository.findById(id);
	}

	public Store save(Store store) {
		return storeRepository.save(store);
	}

	public void deleteById(Integer id) {
		storeRepository.deleteById(id);
	}
}
