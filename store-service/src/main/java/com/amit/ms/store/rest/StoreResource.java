package com.amit.ms.store.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amit.ms.store.domain.Store;
import com.amit.ms.store.service.StoreService;

@RestController
public class StoreResource {

	@Autowired
	private StoreService storeService;

	@GetMapping("/stores")
	public ResponseEntity<List<Store>> findAll() {
		List<Store> stores = storeService.findAll();
		return ResponseEntity.status(HttpStatus.OK).body(stores);
	}

	@GetMapping("/stores/{id}")
	public ResponseEntity<Store> findById(@PathVariable Integer id) {
		Optional<Store> storeOp = storeService.findById(id);
		if (!storeOp.isPresent()) {
			throw new RuntimeException("Store not found " + id);
		}
		return ResponseEntity.status(HttpStatus.OK).body(storeOp.get());
	}

	@PostMapping("/stores")
	public ResponseEntity<Store> save(@RequestBody Store store) {
		Store savedStore = storeService.save(store);
		return ResponseEntity.status(HttpStatus.CREATED).body(savedStore);
	}

	@DeleteMapping("/stores/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Integer id) {
		storeService.deleteById(id);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
}
