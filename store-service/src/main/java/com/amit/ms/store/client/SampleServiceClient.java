package com.amit.ms.store.client;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.amit.ms.store.domain.ResultCalculation;
import com.amit.ms.store.domain.ResultCalculation.Type;

@FeignClient(name = "sample-service")
@RibbonClient(name = "sample-service")
public interface SampleServiceClient {

	@GetMapping("/calculate/{type}/{value1}/{value2}")
	public ResultCalculation calculate(@PathVariable Type type, @PathVariable Double value1,
			@PathVariable Double value2);
}
