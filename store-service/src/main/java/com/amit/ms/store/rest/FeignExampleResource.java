package com.amit.ms.store.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.amit.ms.store.client.SampleServiceClient;
import com.amit.ms.store.domain.ResultCalculation;
import com.amit.ms.store.domain.ResultCalculation.Type;

@RestController
public class FeignExampleResource {

	@Autowired
	private SampleServiceClient sampleClient;

	@GetMapping("/calculate/{type}/{value1}/{value2}")
	public ResultCalculation calculate(@PathVariable("type") Type type, @PathVariable("value1") Double value1,
			@PathVariable("value2") Double value2) {
		ResultCalculation calculate = sampleClient.calculate(type, value1, value2);
		return calculate;

	}
}
