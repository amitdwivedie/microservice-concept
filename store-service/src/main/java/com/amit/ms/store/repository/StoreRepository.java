package com.amit.ms.store.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.amit.ms.store.domain.Store;

public interface StoreRepository extends JpaRepository<Store, Integer> {

}
