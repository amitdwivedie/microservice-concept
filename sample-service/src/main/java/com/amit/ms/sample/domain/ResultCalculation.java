package com.amit.ms.sample.domain;

public class ResultCalculation {

	private Double value1;
	private Double value2;
	private Double result;
	private Type type;
	private String serverPort;

	public ResultCalculation() {

	}

	public ResultCalculation(Double value1, Double value2, Type type) {
		super();
		this.value1 = value1;
		this.value2 = value2;
		this.type = type;
	}

	public enum Type {
		ADD, SUBSTRACT, MULTIPLY, DIVIDE, MOD
	}

	public Double getValue1() {
		return value1;
	}

	public void setValue1(Double value1) {
		this.value1 = value1;
	}

	public Double getValue2() {
		return value2;
	}

	public void setValue2(Double value2) {
		this.value2 = value2;
	}

	public Double getResult() {
		result = 0d;
		if (type != null) {
			switch (type.name()) {
			case "ADD":
				result = value1 + value2;
				break;
			case "SUBSTRACT":
				result = value1 - value2;
				break;
			case "MULTIPLY":
				result = value1 * value2;
				break;
			case "DIVIDE":
				result = value1 / value2;
				break;
			case "MOD":
				result = value1 % value2;
				break;
			default:
				result = 0d;
				break;
			}
		}
		return result;
	}

	public void setResult(Double result) {
		this.result = result;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getServerPort() {
		return serverPort;
	}

	public void setServerPort(String serverPort) {
		this.serverPort = serverPort;
	}

	@Override
	public String toString() {
		return "ResultCalculation [value1=" + value1 + ", value2=" + value2 + ", result=" + result + ", type=" + type
				+ "]";
	}

}
