package com.amit.ms.sample.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amit.ms.sample.domain.ResultCalculation;
import com.amit.ms.sample.domain.ResultCalculation.Type;

@RestController
public class ResultCalculationResource {

	@Autowired
	private Environment env;

	@GetMapping("/calculate/{type}/{value1}/{value2}")
	public ResultCalculation calculate(@PathVariable Type type, @PathVariable Double value1,
			@PathVariable Double value2) {

		String port = env.getProperty("server.port");
		ResultCalculation calculation = new ResultCalculation(value1, value2, type);
		calculation.setServerPort(port);

		return calculation;
	}

	@GetMapping("/calculate")
	public ResultCalculation calculateResult(@RequestBody ResultCalculation resultCalculation) {

		if (resultCalculation.getType() == null || resultCalculation.getValue1() == null
				|| resultCalculation.getValue2() == null) {
			throw new RuntimeException("Please enter the value of value1, value2 and type");
		}
		String port = env.getProperty("server.port");
		resultCalculation.setServerPort(port);

		return resultCalculation;
	}
}
