package com.amit.ms.sample.domain;

public class Sample {

	private String name;
	private Integer size;
	private Integer port;

	public Sample() {
	}

	public Sample(String name, Integer size, Integer port) {
		super();
		this.name = name;
		this.size = size;
		this.port = port;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	public String toString() {
		return "Sample [name=" + name + ", size=" + size + ", port=" + port + "]";
	}

}
