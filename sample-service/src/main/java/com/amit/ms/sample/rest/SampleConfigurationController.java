package com.amit.ms.sample.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.amit.ms.sample.conf.SampleConfiguration;
import com.amit.ms.sample.domain.Sample;

@RestController
public class SampleConfigurationController {

	@Autowired
	private SampleConfiguration sampleConfiguration;
	
	@Autowired
	private Environment environment;
	
	@Value("${spring.application.name}")
	private String name;
	
	@GetMapping("/sample/prop")
	public Sample getSampleConfiguration() {
		int port = Integer.parseInt(environment.getProperty("server.port"));
		return new Sample(sampleConfiguration.getName(), sampleConfiguration.getSize(), port);
	}
	
	@GetMapping("/sample/serviceName")
	public String getServiceName() {
		return name;
	}
}
